EAPI=4

inherit toolchain-funcs flag-o-matic

DESCRIPTION="Nigel's performance MONitor for CPU, memory, network, disks, etc..."
HOMEPAGE="http://nmon.sourceforge.net/"
SRC_URI="https://github.com/baudvix/${PN}/archive/${PV}-r1.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="doc"

DEPEND="sys-libs/ncurses"
RDEPEND="${DEPEND}"

S=${WORKDIR}

src_unpack() {
    unpack ${A}
    [[ -d ${S} ]] || die
}

src_compile() {
    pushd "${WORKDIR}/${PF}" > /dev/null
    	append-cppflags -DJFS -DGETUSER -DLARGEMEM
    	emake CC="$(tc-getCC)" LDLIBS="-lncurses" nmon_x86_64
    popd > /dev/null
}

src_install() {
	pushd "${WORKDIR}/${PF}" > /dev/null
        dobin nmon
        if use doc; then
            insinto /usr/share/doc/${PF}
            doins Documentation.txt || die "Installation of documentation failed"
        fi
    popd > /dev/null
}
